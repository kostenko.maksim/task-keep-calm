<?php

/**
 * Путь куда поместили каталог app
 * обычно $_SERVER['DOCUMENT_ROOT']
 */
define('PATH_APP', 'assets/');
define('PATH_ROOT', '');

if (file_exists('vendor/vendor/autoload.php'))
    require PATH_APP . 'vendor/vendor/autoload.php';

// autoload
spl_autoload_register(
    function ($class) {
        $class = strtolower($class);
        $path = dirname(__FILE__) . '/' . str_replace('\\', '/', $class . '.php');

        if (file_exists($path)) {
            require_once $path;
        }
    }
);



core\Route::Start();