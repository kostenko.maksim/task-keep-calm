<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2"><?= $data['title']?></h1>
    <div>Кол-во найденных закупок: <?=count($data['items']);?></div>
    <div class="btn-toolbar mb-2 mb-md-0">
        <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
        </div>
        <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
        </button>
    </div>
</div>

<!--<canvas class="my-4 w-100" id="myChart" width="900" height="380"></canvas>-->

<?php  \core\Model::prt($data);?>
<div class="table-responsive">
    <table class="table table-striped table-sm">
        <thead>
        <tr>
            <th scope="col">#</th>
            <?php foreach ($data['arTitleHeader'] as $item): ?>
                <th scope="col"><?= $item ?></th>
            <?php endforeach; ?>
        </tr>
        </thead>
        <tbody>
            <?php
            $counter = 0;
            foreach($data['items'] as $arTrade): ?>
                <tr>
                    <td><?= ++$counter ?></td>
                    <?php foreach($arTrade as $key => $tradeValue):
                        if ($key === 'object' || $key === 'link' || $key === 'price')
                            continue;

                        if ($key === 'regNumber'):?>
                          <td><a target="_blank" href="<?=$arTrade['link']?>"><?=$tradeValue?></a></td>
                        <?php continue;
                        endif;
                        ?>
                        <td><?= $tradeValue ?></td>
                    <?php endforeach; ?>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>