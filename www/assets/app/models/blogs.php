<?php
namespace models;

/**
 * Модель статьи
 * Class Blogs
 */
class Blogs extends \core\Model {
    /**
     * @var $id //id модели
     */
    public $id;
    public $name;
    public $text;


    static public $table = 'blogs';

}