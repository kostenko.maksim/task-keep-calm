<?php

namespace core;

class Model
{

    //protected $connection = '';

    function __construct()
    {
        //$this->connection = $this->getConnection();
    }

    public function getConnection()
    {
        try {
            $configDB = require PATH_APP.'app/config/db.conf';
            $connection = new \PDO('mysql:host=' . $configDB['host'] . ';dbname=' . $configDB['dbname'] . ';charset=' . $configDB['charset'], $configDB['user'], $configDB['password']);
            $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return $connection;
        } catch (\PDOEXCEPTION $e) {
            var_dump($e->getMessage());
        }
    }

    public function __get($name)
    {
        $method = 'get' . ucfirst($name);
        return $this->$method();
    }

    public function query($sql, $params = [])
    {
        try {

            $query = $this->connection->prepare($sql);

            /*if (!empty($params)) {
                foreach ($params as $key => $value) {
                    $query->bindValue(':' . $key, $value);
                }
            }*/
            $query->execute($params);
        } catch (\PDOEXCEPTION $e) {
            var_dump($e->getMessage());
            return false;
        }
        return $query;
    }

    public function select($params = [], $limit = null, $start = null)
    {
        $where = '';
        foreach ($params as $key => $param) {
            $where .= $key . ' = ' . $param . ' AND ';
        }
        $where = trim($where, ' AND ');
        $query = 'SELECT * FROM ' . static::$table;
        if ($where) {
            $query .= ' WHERE ' . $where;
        }

        if ($limit) {
            if (!$start) {
                $start = 0;
            }
            $query .= ' LIMIT ' . $start . ',' . $limit;
        }
        $result = $this->query($query, $params);
        return $result->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function save()
    {
        if (!$this->id) {
            if (method_exists($this, 'beforeSave')) {
                $this->beforeSave();
            }
        }
        $properties = $this->getArray();

        // очистим массив свойств от пустых значений
        $properties = array_diff($properties, array(''));

        $query = ($this->id) ? 'UPDATE ' . static::$table . ' SET ' : 'INSERT ' . static::$table . ' SET ';
        foreach ($properties as $key => $value) {
            $query .= $key . '=:' . $key . ', ';
        }
        $query = trim($query, ', ');
        $query .= ($this->id) ? ' WHERE id=' . $this->id : '';

        try {
            $this->query($query, $properties);
            if (!$this->id) {
                // передаем в модель id нового пользователя
                $this->id = $this->connection->lastInsertId();
            }
            return true;
        } catch (\PDOEXCEPTION $e) {
            var_dump($e->getMessage());
            return false;
        }
    }

    public function load($response)
    {
        foreach ($response as $property => $value) {
            if (property_exists(get_class($this), $property)) {
                $this->$property = $value;
            }
        }
    }

    static function getOne($params = [])
    {
        $model = new static();
        $where = '';
        foreach ($params as $key => $param) {
            $where .= $key . ' = :' . $key .' AND ';
        }
        $where = trim($where, ' AND ');
        $query = 'SELECT * FROM ' . static::$table;
        if ($where) {
            $query .= ' WHERE ' . $where . ' LIMIT 0,1';
        }
        $result = $model->query($query, $params);
        $sqlResponse = $result->fetchAll(\PDO::FETCH_ASSOC);
        if (!empty($sqlResponse)) {
            $model->load($sqlResponse[0]);
            return $model;
        }
    }

    static function getCount($params = [])
    {
        $model = new static();
        $where = '';
        foreach ($params as $key => $param) {
            $where .= $key . ' = :' . $key .' AND ';
        }
        $where = trim($where, ' AND ');
        $query = 'SELECT COUNT(*) FROM ' . static::$table;
        if ($where) {
            $query .= ' WHERE ' . $where;
        }
        $result = $model->query($query, $params);
        return $result->fetchColumn();
    }

    public function getArray()
    {
        return get_object_vars($this);
    }

    static function getPagination($limit, $page, $params = [])
    {
        $data = [];

        $count = self::getCount($params);
        $totalPage = ceil($count / $limit);

        if ($page > $totalPage) {
            $page = $totalPage;
        }

        $start = $page * $limit - $limit;

        $data['count'] = $count;
        $data['page'] = $page;
        $data['limit'] = $limit;
        $data['totalPage'] = $totalPage;
        $data['start'] = $start;

        return $data;
    }

    static function getList($params = [], $limit = null, $start = null)
    {
        $return_list = [];
        $model = new static();
        $sqlResponse = $model->select($params, $limit, $start);
        foreach ($sqlResponse as $sqlResponseItem) {
            $object = new static();
            $object->load($sqlResponseItem);
            $return_list[] = $object;
        }
        return $return_list;
    }

    public function checkConfirm($value1, $value2)
    {
        return $value1 == $value2;
    }

    public function validation($arrFields)
    {
        $errors = [];
        $arrValidationRules = static::$arrValidationRules;
        $arrErrors = static::$arrValidationErrors;

        foreach ($arrFields as $key => $value) {
            if (array_key_exists($key, $arrValidationRules)) {
                switch ($arrValidationRules[$key]['type']) {
                    case 'pattern':
                        if (preg_match($arrValidationRules[$key]['value'], $value) != 1) {
                            $errors[$key] = $arrErrors[$key];
                        }
                        break;

                    case 'method':
                        $nameMethod = $arrValidationRules[$key]['value'];
                        if (method_exists($this, $nameMethod)) {
                            if (!$this->$nameMethod(
                                $arrFields[$key],
                                $arrFields[$arrValidationRules[$key]['field2']]
                            )) {
                                $errors[$key] = $arrErrors[$key];
                            }
                        }
                        break;
                }
            }
        }
        return $errors;
    }

    /**
     * @param string $data
     */
    static function prt($data = '')
    {
        print_r('<pre>');
        print_r($data);
        print_r('</pre>');
    }
}