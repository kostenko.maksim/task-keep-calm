<?php

namespace controllers;

use core\Controller;
use models\User;
use models\Blogs;


class Controller_Main extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->data['errors'] = [];
        $this->data['historyLog'] = [];

    }

    function action_index()
    {
        $this->data['title'] = 'Главная';

        $layout = static::isAjax() ? 'json.php' : 'template_view.php';
        $this->view->generate('main_view.php', $layout, $this->data);
    }

    public function render()
    {
        $this->view->generate('json.php', 'template_ajax.php', $this->data);
    }



}