<?php


namespace models;


use Bitrix\Main\Entity\Query\Filter\Expression\NullEx;
use core\Model;

class Comments extends Model
{
    public $id;
    public $user_id;
    public $blog_id;
    public $text;
    public $date;

    static public $table = 'blogs_comments';

    static function getComments($blog_id = NULL,$limit = NULL)
    {
        if (!$blog_id)
            return [];

        $params = ['blog_id' => $blog_id];
        $query = 'SELECT blogs_comments.id,blogs_comments.date AS dateComment,blogs_comments.text,users.name AS author 
                  FROM blogs_comments JOIN users ON blogs_comments.user_id = users.id 
                  WHERE blogs_comments.blog_id = :blog_id ORDER BY dateComment';
        $start = 1;
        if ($limit)
            $query .= ' LIMIT 0,'.$limit;

        $model = new Model();
        $result = $model->query($query, $params);

        return $result->fetchAll(\PDO::FETCH_ASSOC);


    }

}