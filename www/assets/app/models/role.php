<?php

namespace models;

/**
 * Модель ролей - права доступа
 * Class Role
 */
class Role extends \core\Model
{
    /**
     * @var $id //id роли
     */
    public $id;
    public $name;


    static public $table = 'roles';

    public function getRole($role_id)
    {
        return static::getOne(['id' => $role_id]);
    }


}