/*
  autoprefixer - https://github.com/postcss/autoprefixer
  cssnano - https://github.com/cssnano/cssnano
  css-mqpacker - HAS BEEN REMOVED! Do not use!
*/

module.exports = {
  plugins: [
    require('autoprefixer'),
    require('cssnano')({
      ttt: 'dsds',
      preset: [
        'default', {
          discardComments: {
            removeAll: true
          }
        }
      ]
    })
  ]
}
