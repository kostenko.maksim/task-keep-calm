<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $data['title'] ?></title>
    <link rel="stylesheet"
          href="/assets/css/app.css?v=<?= filemtime($_SERVER['DOCUMENT_ROOT'] . '/assets/css/app.css') ?>">
</head>
<body>
<header class="navbar navbar-dark sticky-top bg-dark d-flex flex-md-nowrap p-0 shadow">
    <button class="navbar-toggler d-md-none collapsed" type="button" data-bs-toggle="collapse"
            data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3 d-none d-sm-block" href="/">Task</a>

    <div class="navbar-nav flex-row">
        <?php if (isset($data['authorized']) && $data['authorized']): ?>
            <li class="nav-item">
                <a class="nav-link" href="/profile"><?= $data['user']->name . ' ' . $data['user']->lastname ?>
                    (<?= $data['user']->role->name ?>)</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/profile/logout?back-url=<?=$_SERVER['REQUEST_URI'];?>">Выйти</a>
            </li>

        <?php else: ?>
            <li class="nav-item">
                <a class="nav-link" href="/profile/register">Регистрация</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/profile/login">Войти</a>
            </li>
        <?php endif; ?>


    </div>
</header>

<div class="container-fluid flex-grow-1">
    <div class="row h-100">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse h-100">
            <div class="position-sticky pt-3">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link <?= empty($data['activePage']) ? 'active' : '' ?>" aria-current="page"
                           href="/">
                            <span data-feather="download-cloud"></span>
                            Главная
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?= $data['activePage'] == 'blogs' ? 'active' : '' ?>" href="/blogs">
                            <span data-feather="activity"></span>
                            Статьи
                        </a>
                    </li>

                </ul>

            </div>
        </nav>

        <main id="content" class="col-md-9 ms-sm-auto col-lg-10 px-md-4 mt-2 flex-column">
            <?php include PATH_APP . 'app/views/' . $content_view; ?>
        </main>
    </div>

</div>
<footer class="footer bg-dark text-light">
    <div class="copyright">Copyright &copy; <?= date('Y') ?></div>
</footer>

<script src="/assets/js/app.js?v=<?= filemtime($_SERVER['DOCUMENT_ROOT'] . '/assets/js/app.js') ?>"></script>

</body>
</html>