<div id="comments" class="comments mb-5">
    <div class="comments__title">
        Комментарии (<?= count($data['comments']) ?>):
    </div>

    <?php foreach ($data['comments'] as $item): ?>
        <div class="comment">
            <div class="comment__title">
                <span><?= $item['author']; ?> #<?= $item['id']; ?></span>
                <span><?= $item['dateComment']; ?></span>
            </div>
            <div class="comment__text">
                <?= $item['text']; ?>
            </div>
        </div>

    <?php endforeach; ?>

    <?php if ($data['authorized']): ?>
        <form class="form-add-comment text-center w-100 mb-4">
            <textarea class="w-100" name="comment" id="comment" cols="30" rows="4"></textarea>
            <button type="submit" class="btn btn-dark" data-tag="#comments" data-limit="<?= $data['limit'] ?>">
                Отправить
            </button>
        </form>
    <?php else: ?>
        <div class="link-login">
            Чтобы оставить комментарий пройдите <a href="/profile/login?back-url=<?=$_SERVER['REQUEST_URI'];?>">Авторизацию</a>
        </div>

    <?php endif; ?>

    <?php  //\core\Model::prt($data);
    if ($data['totalPage'] && $data['totalPage'] != 1): ?>
        <div class="text-center">
            <button class="page-link-more btn btn-dark" data-tag="#comments" data-limit="<?= $data['limit'] ?>">Показать
                ещё
            </button>
        </div>
    <?php endif; ?>





</div>
