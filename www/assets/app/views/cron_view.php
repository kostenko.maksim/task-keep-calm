<?php
$bufferErrors = ob_get_contents();

if (mb_strlen($bufferErrors) > 0)
    \models\email::sendEmail($bufferErrors);

ob_end_clean(); //убрать если что то не работает, для отображения ошибок PHP в ответе ajax

header('Content-Type: application/json; charset=UTF-8');
//unset($data['historyLog']);
echo json_encode($data);