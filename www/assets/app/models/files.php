<?php


namespace models;


class files
{
    /**
     *
     *
     */
    static function moveXML()
    {
        if (!file_exists(PATH_ROOT.'xml/trade_load.xml')) {
            echo "Файла не существует : ".PATH_ROOT.'xml/trade_load.xml';
            return;
        }

        try {
            rename(PATH_ROOT.'xml/trade_load.xml', PATH_ROOT.'xml/old/' . date("Ymd-Hi") . '.xml');
        } catch (Exception $e) {
            echo "Moved XML file Error: {$e->getMessage()}";
        }

    }

}

