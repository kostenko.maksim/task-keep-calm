<?php


namespace controllers;

use core\Controller,
    core\View,
    core\Model,
    models\Blogs,
    models\Comments,
    models\User;
use core\Route;


class Controller_Blog extends Controller
{
    function action_index() {


        $this->data['item'] = Blogs::getOne(['id' => $_GET['id']]);

        if (empty($this->data['item']))
            Route::ErrorPage404();

        $this->data['title'] = $this->data['item']->name;

        $limit = 3;
        if (isset($this->request['limit']) && $this->request['limit']>0) {
            $limit = $this->request['limit'];
        }
        $page = 1;
        if (isset($this->request['page']) && $this->request['page']>0) {
            $page = $this->request['page'];
        }
        $this->data = array_merge($this->data, Comments::getPagination($limit, $page, ['blog_id' => $_GET['id']]));


        $this->data['comments'] = Comments::getComments($this->data['item']->id, $limit);
        //Model::prt($this->data['comments']);


        //$this->data['comments'] = Comments::getComments();
        //$this->view->generate('blog_view.php','template_view.php',$this->data);

        $layout = static::isAjax() ? 'template_ajax.php' : 'template_view.php';
        $this->view->generate('blog_view.php', $layout, $this->data);
    }

}