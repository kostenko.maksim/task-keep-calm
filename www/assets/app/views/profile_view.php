<?php if($data['authorized']):?>
    <?php
    $dataEdit['user'] = $data['user'];
    if (isset($data['user-edit'])) {
        $dataEdit['user'] = $data['user-edit'];
    }
    ?>
        <h3>Данные профиля:</h3>
        <div class="container h-100">
            <div class="row justify-content-center align-items-center">
                <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4 border rounded">
                    <form class="form-update" method="post">
                        <?php //<input type="hidden" name="action" value="update">?>
                        <input type="hidden" name="id" value="<?=$dataEdit['user']->id?>">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Имя" value="<?=isset($dataEdit['user']->name) ? $dataEdit['user']->name : ''?>">
                            <div class="form-group">
                                <span class="text-center text-danger error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="lastname" placeholder="Фамилия" value="<?=isset($dataEdit['user']->lastname) ? $dataEdit['user']->lastname : ''?>">
                            <div class="form-group">
                                <span class="text-center text-danger error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="email" placeholder="E-mail" value="<?=$dataEdit['user']->email?>" disabled>
                            <div class="form-group">
                                <span class="text-center text-danger error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="phone" placeholder="Телефон" value="<?=isset($dataEdit['user']->phone) ? $dataEdit['user']->phone : ''?>">
                            <div class="form-group">
                                <span class="text-center text-danger error"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="address" placeholder="Адрес" value="<?=isset($dataEdit['user']->address) ? $dataEdit['user']->address : ''?>">
                            <div class="form-group">
                                <span class="text-center text-danger error"></span>
                            </div>
                        </div>
                        <?php if(isset($data['roles'])):?>
                            <div class="form-group">
                                <select name="role_id" class="custom-select">
                                    <?php foreach ($data['roles'] as $key => $value): ?>
                                        <option <?=($dataEdit['user']->role_id == $value->id) ? 'selected' : ''?> value="<?=$value->id?>"><?=$value->name?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        <?php endif;?>
                        <div class="form-group">
                            <span class="text-center text-danger msg"></span>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>

                </div>
            </div>
        </div>

<?php else:?>
    <h3>Для доступа вам необходимо авторизоваться</h3>
    <a class="btn btn-primary" href="/profile/login">Войти</a>
    <a class="btn btn-primary" href="/profile/register">Регистрация</a>
<?php endif;?>