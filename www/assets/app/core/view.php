<?php

namespace core;

class View
{

    function generate($content_view, $template_view, $data = null)
    {
        include PATH_APP . 'app/views/' . $template_view;
    }

    static function render($template_view, $data = null)
    {
        include PATH_APP . 'app/views/' . $template_view;
    }
}