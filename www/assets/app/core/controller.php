<?php

namespace core;

use models\Role;
use models\User;

class Controller
{
    public $view;
    public $route;
    public $request;
    public $data;

    function __construct()
    {
        $this->view = new View();
        $this->route = new Route();

        $this->data['activePage'] = $this->route->activePage;
        $this->data['authorized'] = User::authorized();

        if (isset($this->data['authorized']) && $this->data['authorized']) {
            $this->data['user'] = User::getOne(['id' => $_SESSION['userAuth']]);
        }

        foreach ($_REQUEST as $key => $value) {
            // добавляем слеши перед кавычками, чтобы они стали виде escape-последовательности,
            $value = addslashes($value);
            // заменяем все специальные символы эквивалентом
            $value = htmlspecialchars($value);
            $this->request[$key] = $value;
        }
    }

    static function isAjax()
    {
        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower(
                $_SERVER['HTTP_X_REQUESTED_WITH']
            ) == 'xmlhttprequest') {
            return true;
        }
        return false;
    }

}