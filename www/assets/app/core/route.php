<?php

namespace core;

class Route
{
    public $activePage;

    public function __construct()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $withoutParams = strstr($_SERVER['REQUEST_URI'], '?', true);
            if ($withoutParams) {
                $uri = explode('/', $withoutParams);
            } else {
                $uri = explode('/', $_SERVER['REQUEST_URI']);
            }
            $this->activePage = $uri[1];
        }
    }

    static function Start()
    {
        // контроллер и действие по умолчанию
        $controller_name = 'Main';
        $action_name = 'index';
        // исключаем GET параметры из REQUEST_URI
        $withoutParams = strstr($_SERVER['REQUEST_URI'], '?', true);
        if ($withoutParams) {
            $routes_uri = explode('/', $withoutParams);
        } else {
            $routes_uri = explode('/', $_SERVER['REQUEST_URI']);
        }

        // получаем имя контроллера из REQUEST_URI
        if (!empty($routes_uri[1])) {
            $controller_name = $routes_uri[1];
        }
        // получаем имя экшена из REQUEST_URI
        if (!empty($routes_uri[2])) {
            $action_name = $routes_uri[2];
        }

        // добавляем префиксы
        $controller_name = 'Controller_' . $controller_name;
        $action_name = 'action_' . $action_name;

        // проверка маршрута по файлу контроллеру
        $controller_file = strtolower($controller_name) . '.php';
        $controller_path = PATH_APP . '/app/controllers/' . $controller_file;
        if (!file_exists($controller_path)) {
            Route::ErrorPage404('routing_error');
        }

        // добавляем область видимости
        $controller_name = '\controllers\\' . $controller_name;
        // создаем контроллер
        $controller = new $controller_name;
        //$controller = new \controllers\Controller_Main;
        $action = $action_name;

        if (method_exists($controller, $action)) {
            // вызываем действие контроллера
            $controller->$action();
        } else {
            Route::ErrorPage404('method_not_exists');
        }
    }

    static function ErrorPage404($tag = null)
    {
        http_response_code(404);
        $controller = new \controllers\Controller_404;
        $controller->data['error-404'] = $tag;
        // вызываем действие контроллера
        $controller->action_index();
        die();
    }
}