 <div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4 border rounded">
            <form class="form-register" method="post">
                <input type="hidden" name="action" value="register">
                <div class="form-group">
                    <input type="text" class="form-control" name="name" placeholder="Имя">
                    <div class="form-group">
                        <span class="text-center text-danger error"></span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="lastname" placeholder="Фамилия">
                    <div class="form-group">
                        <span class="text-center text-danger error"></span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" name="email" placeholder="E-mail">
                    <div class="form-group">
                        <span class="text-center text-danger error"></span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Пароль">
                    <div class="form-group">
                        <span class="text-center text-danger error"></span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="password_confirm" placeholder="Подтверждение пароля">
                    <div class="form-group">
                        <span class="text-center text-danger error"></span>
                    </div>
                </div>
                <div class="form-group">
                    <span class="text-center text-danger msg"></span>
                </div>
                <button type="submit" class="btn btn-primary">Зарегестрироваться</button>
            </form>

        </div>
    </div>
 </div>