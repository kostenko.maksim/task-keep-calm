<?php core\Model::prt($data); core\Model::prt($_GET);?>
<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form mt-5 p-4 border rounded">
                <h1>Авторизация</h1>
                <?php if(isset($data['authorized']) && $data['authorized']):?>
                    <span>Вы уже авторизированны</span>
                    <a class="btn btn-primary" href="/profile/logout">Выйти</a>
                <?php else:?>
                <form class="form-login" method="post">
                    <input type="hidden" name="action" value="login">
                    <?php if (isset($_GET['back-url'])):?>
                        <input type="hidden" name="back-url" value="<?=$_GET['back-url']?>">
                    <?php endif; ?>
                    <div class="form-group mb-2">
                        <input type="email" class="form-control" name="email" placeholder="Ваш e-mail" required>
                    </div>
                    <div class="form-group mb-2">
                        <input type="password" class="form-control" name="password" placeholder="Ваш пароль" required>
                    </div>

                    <div class="form-group mb-1">
                        <span class="text-center text-danger msg"></span>
                    </div>

                    <button type="submit" class="btn btn-primary login">Войти</button>
                </form>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>
