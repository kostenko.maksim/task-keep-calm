<?php

namespace models;

/**
 * Модель пользователя
 * Class User
 */
class User extends \core\Model
{
    /**
     * @var $id //id модели
     */
    public $id;
    public $email;
    public $phone;
    public $name;
    public $lastname;
    public $address;
    public $salt;
    public $password;
    public $role_id = 2;

    static public $table = 'users';
    static public $arrValidationRules = [
        'name' => [
            'type' => 'pattern',
            'value' => '/^[A-Za-zА-Яа-я0-9ё\s\w,.]{2,}$/u'
            // может содержать только буквенно-числовые символы, пробел, знаки препинания
        ],
        'lastname' => [
            'type' => 'pattern',
            'value' => '/^[A-Za-zА-Яа-я0-9ё\s\w,.]{2,}$/u'
            // может содержать только буквенно-числовые символы, пробел, знаки препинания
        ],
        'phone' => [
            'type' => 'pattern',
            'value' => '#^((\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{10,10}$#', // 10 цифр и перед ними +7
        ],
        'email' => [
            'type' => 'pattern',
            'value' => '/^((([0-9A-Za-z]{1}[-0-9A-z\.]{1,}[0-9A-Za-z]{1})|([0-9А-Яа-я]{1}[-0-9А-я\.]{1,}[0-9А-Яа-я]{1}))@([-A-Za-z]{1,}\.){1,2}[-A-Za-z]{2,})$/u',
            // список разрешенных символов знаки "@" и "." и "-" и "_"
        ],
        'address' => [
            'type' => 'pattern',
            'value' => '/^[A-Za-zА-Яа-я0-9ё\s\w,-.\/]{2,}$/u',
        ],
        'password' => [
            'type' => 'pattern',
            'value' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[!@#$%^&*,.])[a-zA-Z!@#$%^&*,.\d]{8,}$/',
            //длина от 8, [a-zA-Z], спецсимволы
        ],
        'password_confirm' => [
            'type' => 'method',
            'value' => 'checkConfirm',
            'field2' => 'password'
        ],
    ];
    static public array $arrValidationErrors = [
        'name' => 'поле заполнено некорректно',
        'lastname' => 'поле заполнено некорректно',
        'email' => 'поле заполнено некорректно',
        'phone' => 'поле заполнено некорректно: 10 цифр и перед ними +7',
        'address' => 'поле заполнено некорректно',
        'password' => 'Пароль должен соотвтетствовать сложности: длина не менее 8 символов, должны использоваться буквы обоих регистров, знаки препинания и цифры',
        'password_confirm' => 'Подтверждение пароля не совпадает',
    ];

    static public array $relations = [
        'role' => [
            'model' => 'models\Role',
            'connector' => 'role_id'
        ]
    ];

    public function getRole(): Role
    {
        return Role::getOne(['id' => $this->role_id]);
    }

    static function authorized()
    {
        return isset($_SESSION['userAuth']);
    }

    static function isAdmin()
    {
        if (self::authorized()) {
            return $_SESSION['userAuthRole'] == 'Administrator';
        }
        return false;
    }

    public function beforeSave()
    {
        $this->salt = $this->generateSalt();
        $this->password = md5($this->password . $this->salt);
    }

    public function generateSalt()
    {
        $salt = '';
        $saltLength = 8; //длина соли
        for ($i = 0; $i < $saltLength; $i++) {
            $salt .= chr(mt_rand(33, 126)); //символ из ASCII-table
        }
        return $salt;
    }

}