<?php if (!empty($data['list'])): ?>
    <h3>Статьи</h3>
    <table class="table table-striped result table-hover">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">id</th>
            <th scope="col">Название</th>
            <th scope="col">Текст</th>


        </tr>
        </thead>
        <tbody>
        <?php foreach ($data['list'] as $key => $value): ?>
            <tr>
                <th scope="row"><?= ($key + 1) ?></th>
                <td><?= $value->id ?></td>
                <td><a href="/blog?id=<?= $value->id ?>"><?= $value->name ?></a></td>
                <td><?= mb_strimwidth($value->text, 0, 120, '...'); ?></td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <?php if ($data['totalPage'] && $data['totalPage'] != 1): ?>
        <nav aria-label="navigation" class="text-center">
            <?php $this->render('pagination_view.php', $data); ?>
        </nav>
    <?php endif; ?>

<?php endif; ?>