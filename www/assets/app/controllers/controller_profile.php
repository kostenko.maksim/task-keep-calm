<?php

namespace controllers;

use core\Controller;
use models\Role;
use models\User;

class Controller_Profile extends Controller
{

    function action_index()
    {
        $this->data['title'] = 'Личный кабинет';

        if (isset($this->request['id']) && User::isAdmin()) {
            $user = User::getOne(['id' => $this->request['id']]);
            if ($user) {
                $this->data['user-edit'] = $user;
            }
        }

        if (User::isAdmin()) {
            $this->data['roles'] = Role::getList();
        }

        if (static::isAjax()) {
            $success = $this->update();
            echo json_encode(
                ['success' => $success, 'msg' => $this->data['message'], 'dataErrors' => $this->data['errors']]
            );
        } else {
            $this->view->generate('profile_view.php', 'template_view.php', $this->data);
        }
    }

    function action_logout()
    {
        $this->data['title'] = 'Авторизация';
        $this->logout();
        $this->view->generate('login_view.php', 'template_view.php', $this->data);
    }

    function action_login()
    {
        $this->data['title'] = 'Авторизация';

        if (static::isAjax()) {
            if ($this->login()) {
                echo json_encode(['success' => 1, 'msg' => 'Вы успешно авторизовались']);
            } else {
                echo json_encode(['success' => 0, 'msg' => 'Неправильный логин или пароль']);
            }
        } else {
            $this->view->generate('login_view.php', 'template_view.php', $this->data);
        }
    }

    function action_register()
    {
        $this->data['title'] = 'Регистрация';
        if (static::isAjax()) {
            $success = $this->register();
            echo json_encode(
                ['success' => $success, 'msg' => $this->data['message'], 'dataErrors' => $this->data['errors']]
            );
        } else {
            $this->view->generate('register_view.php', 'template_view.php', $this->data);
        }
    }

    public function login()
    {
        $return = false;
        $email = $this->request['email'];
        $password = $this->request['password'];
        $user = User::getOne(['email' => $email]);

        if (!empty($user)) {
            $password = md5($password . $user->salt);
            if ($password == $user->password) {
                $user->role = $user->getRole($user->id);
                $_SESSION['userAuth'] = $user->id;
                $_SESSION['userAuthRole'] = $user->role->name;
                $return = true;
            }
        }
        return $return;
    }

    public function logout()
    {
        $backURL = $_GET['back-url'] ?: '/';
        if (isset($_SESSION['userAuth']) && $_SESSION['userAuth']) {
            session_destroy();
            header('location: '.$backURL);
        }
    }

    private function register()
    {
        $this->data['message'] = "Ошибка регистрации";
        if (!empty($this->request)) {
            $model = new User();

            $this->data['errors'] = $model->validation($this->request);
            if (!empty($this->data['errors'])) {
                return false;
            }

            $user = User::getOne(['email' => $this->request['email']]);
            if (!empty($user)) {
                $this->data['message'] = "Ошибка: пользователь уже существуют";
                return false;
            }
            $model->load($this->request);
            $model->save();

            $this->data['message'] = "Успешная регистрация";
            return true;
        }

        return false;
    }

    private function update()
    {
        $this->data['message'] = "Ошибка сохранения";
        if (!empty($this->request)) {
            $user = new User();
            $this->data['errors'] = $user->validation($this->request);
            if (!empty($this->data['errors'])) {
                return false;
            }

            $user->load($this->request);

            if ($user->save()) {
                $this->data['message'] = "Данные обновлены";
                if ($_SESSION['userAuth'] == $this->request['id']) {
                    $this->data['user'] = $user;
                }
                return true;
            }
        }
        return false;
    }
}