<?php if (!empty($data['item'])): ?>

    <h3 class="h3"><?= $data['item']->name; ?></h3>
    <div class="text-body">
        <?= $data['item']->text ?>
    </div>

    <div>
        <a class="btn btn-dark mt-1" href="/blogs">Назад</a>
    </div>

    <?php \core\View::render('comments_view.php',$data);?>

<?php endif; ?>