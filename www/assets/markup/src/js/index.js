import $ from "jquery";
import '@popperjs/core';
import 'bootstrap';

$(document).ready(function() {

    $(document).on('click', 'a.page-link', function (event) {
        event.preventDefault();

        let url = $(this).attr('href');

        $.ajax({
            type: "POST",
            url: url,
        }).done(function(res) {
            $('#content').html(res);
            if (history.pushState) {
                let baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
                let newUrl = baseUrl+url;
                history.pushState(null, null, newUrl);
            }
            else {
                console.warn('History API не поддерживается');
            }
        });
    })

    $(document).on('click', '.page-link-more', function (event) {
        event.preventDefault();

        let limit = Number($(this).data('limit'))+3,
            baseUrl = window.location.protocol + "//" + window.location.host + window.location.pathname,
            objParams = getParamsObj();

        objParams.limit = limit;

        let strParams = getStrParamsObj(objParams),
            newUrl = baseUrl+strParams;

        $.ajax({
            type: "POST",
            url: newUrl,
        }).done(function(res) {
            $('#content').html(res);
            if (history.pushState) {
                history.pushState(null, null, newUrl);
            }
            else {
                console.warn('History API не поддерживается');
            }
        });
    })

    $(document).on('submit', 'form.form-add-comment', function (event) {
        event.preventDefault();
        let dataForm = $(this).serialize();
        $.ajax({
            type: "POST",
            data: dataForm
        }).done(function(res) {
            res = JSON.parse(res);
            if (!res.success) {
                $('span.msg').text(res.msg);
            } else {
                $('span.msg').text(res.msg);
                window.location.href = '/';
            }
        });
    });

    $(document).on('submit', 'form.form-login', function (event) {
        event.preventDefault();
        let dataForm = $(this).serialize();
        let $backURL = $('input[name=back-url]');
        let backURL = $backURL.length > 0 ? $backURL.val() : '/'
        $.ajax({
            type: "POST",
            data: dataForm
        }).done(function(res) {
            res = JSON.parse(res);
            if (!res.success) {
                $('span.msg').text(res.msg);
            } else {
                $('span.msg').text(res.msg);
                window.location.href = backURL;
            }
        });
    });

    $(document).on('input', 'form.form-login input', function (event) {
        if ($('span.msg').text().length > 0) {
            $('span.msg').text('');
        }
    });

    $(document).on('submit', 'form.form-register', function (event) {
        event.preventDefault();
        let dataForm = $('form.form-register').serialize();
        $.ajax({
            type: "POST",
            data: dataForm
        }).done(function(res) {
            $('span.error').text('');
            res = JSON.parse(res);

            if (!res.success) {
                for (let key in res.dataErrors) {
                    $('input[name='+key+']').next().find('span.error').text(res.dataErrors[key]);
                }
                /*for (key in res.dataErrors) {
                    $('span.error-'+key).text(res.dataErrors[key]);
                }*/
            } else {
                $('form.form-register').trigger('reset');

            }
            $('span.msg').text(res.msg);
        });
    });

    $(document).on('input', 'form.form-register input', function (event) {

        if ($(this).next().find('span.error').text().length > 0) {
            $(this).next().find('span.error').text('');
        }

        if ($('span.msg').text().length > 0) {
            $('span.msg').text('');
        }
    });

    $(document).on('submit', 'form.form-update', function (event) {
        event.preventDefault();
        let dataForm = $('form.form-update').serialize();

        $.ajax({
            type: "POST",
            data: dataForm
        }).done(function(res) {
            $('span.error').text('');
            res = JSON.parse(res);

            if (!res.success) {
                for (key in res.dataErrors) {
                    $('input[name='+key+']').next().find('span.error').text(res.dataErrors[key]);
                }
            }
            $('span.msg').text(res.msg);
        });
    });



    $(document).on('click','.submit', function (event) {
        event.preventDefault();
        let postData = {},
            $phone = $('#phone'),
            count = $phone.data('count'),
            phoneValue = $phone.val().replace(/[^0-9.]/g, "");

        if (phoneValue.length == 0)
            return;

        postData.phone = phoneValue;
        postData.count = ++count;

        $('.loader').addClass('loader--active');

        $.ajax({
                method: "POST",
                data: postData
        }).done(function( html ) {
                //$('.ajax-result').html(html);
                $('.loader').removeClass('loader--active');
                $('.ajax__version').text('Данные обновлены с помощью ajax ver.'+postData.count);

                let $phone = $('#phone');
                $phone.data('count',postData.count);
                console.log($phone.val());
                // фиксим баг Inputmask и ajax когда мы передаем в поле 11 цифр
                // описание бага: вначало номера добавлялась 7 со смещением при каждом onAjaxSuccess
                if ($phone.val().length == 11)
                    $phone.val($phone.val().substr(1));

                $phone.inputmask({
                    mask: "+7 (*99) 999-99-99",
                    showMaskOnHover: false,
                    definitions: { '*': { "validator": "[9]" }}
                });
        });
    })

    $(document).on('click','.open-modal', function (event) {
        event.preventDefault();
        $('.modal__overall').toggleClass('modal__overall--show');
        $('.modal__dialog').show();
        $('body').css('overflow','hidden');
    });

    $(document).on('click','.modal__overall, .close-modal', function (event) {
        console.log('click');
        event.preventDefault();
        $('.modal__overall').toggleClass('modal__overall--show');
        $('.modal__dialog').hide();
        $('body').css('overflow','inherit');
    });

});


// преобразуем строку из GET-параметров в объект
function getParamsObj() {
    let params = window.location.search
        .replace('?','')
        .split('&')
        .reduce(
            function(p,e){
                var a = e.split('=');
                p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                return p;
            },
            {}
        );
    return params;
}

// преобразуем объект из GET-параметров в строку
function getStrParamsObj(objParams) {
    let strParams = '?';
    for (let key in objParams) {
        strParams += key+'='+objParams[key]+'&'
    }
    return strParams.slice(0, -1);
}





