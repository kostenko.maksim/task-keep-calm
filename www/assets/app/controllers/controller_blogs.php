<?php


namespace controllers;

use core\Controller,
    models\Blogs;

class Controller_Blogs extends Controller
{
    function action_index() {
        $this->data['title'] = 'Статьи';

        $limit = 5;
        if (isset($this->request['limit']) && $this->request['limit']>0) {
            $limit = $this->request['limit'];
        }
        $page = 1;
        if (isset($this->request['page']) && $this->request['page']>0) {
            $page = $this->request['page'];
        }
        $this->data = array_merge($this->data, Blogs::getPagination($limit, $page));

        $this->data['list'] = Blogs::getList([],$limit,$this->data['start']);
        //$this->view->generate('blogs_view.php','template_view.php',$this->data);

        $layout = static::isAjax() ? 'template_ajax.php' : 'template_view.php';
        $this->view->generate('blogs_view.php', $layout, $this->data);

    }

}