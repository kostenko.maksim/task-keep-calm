<ul class="pagination justify-content-center">
    <?php if($data['page'] != 1):?>
        <li class="page-item"><a class="page-link" href="?page=1" title="к первой записи">&laquo;</a></li>
    <?php endif;?>

    <?php for($i = 1; $i <= $data['totalPage']; $i++):?>
        <li class="page-item <?=($data['page'] == $i) ? 'active' : ''?>"><a class="page-link" href="?page=<?=$i?>"><?=$i?></a></li>
    <?php endfor;?>

    <?php if($data['page'] != $data['totalPage']):?>
        <li class="page-item"><a class="page-link" href="?page=<?=$data['totalPage']?>" title="к последней странице -> <?=$data['totalPage']?>">&raquo;</a></li>
    <?php endif;?>
</ul>
