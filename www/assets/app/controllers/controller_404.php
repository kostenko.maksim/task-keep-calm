<?php

namespace controllers;

use core\Controller;

class Controller_404 extends Controller
{
    //public $data;

    function action_index()
    {
        $this->data['title'] = 'Страница не найдена';
        $this->view->generate('404_view.php', 'template_view.php', $this->data);
    }

}